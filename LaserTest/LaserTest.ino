#define INPUTPIN 2
#define ALARMPIN 4

void setup() 
{
  pinMode(INPUTPIN, INPUT);
  pinMode(ALARMPIN, OUTPUT);
    
  digitalWrite(ALARMPIN, HIGH); 
}

void loop() 
{
  int val = digitalRead(INPUTPIN);
  if(val)
  {
    digitalWrite(ALARMPIN, HIGH); 
  }
  else
  {
    digitalWrite(ALARMPIN, LOW);
    delay(2000);
  }
}
