#ifndef SSD1306OLEDDEBUG_HEADER
#define SSD1306OLEDDEBUG_HEADER

#include <U8glib.h>
#include <Arduino.h>


/*
   1. Line height is the vertical font size
      in pixels (9), plus the spacing between
      lines (1).

	2. Display height is the height of the display
     in pixels.
*/
namespace constants
{
	const int NUMBER_OF_LINES = 6;
  const int CHARACTERS_PER_LINE = 22;
	const int LINE_HEIGHT = 10;
	const int DISPLAY_HEIGHT = 64;
}

class SSD1306OledDebug
{
	public:
    SSD1306OledDebug(int rPin, int hOffset);
    SSD1306OledDebug(int rPin);
		void clearLines();
		void appendLine(char * line);

  private:
		char lines[constants::NUMBER_OF_LINES][constants::CHARACTERS_PER_LINE];
    U8GLIB_SSD1306_128X64 * u8g;

    int resetPin;
		int horizontalOffset;

		void draw(void);
		void init(int rPin, int hOffset);
};

#endif
