#include <SSD1306OledDebug.h>


SSD1306OledDebug::SSD1306OledDebug(int rPin, int hOffset)
{
	init(rPin, hOffset);	
}

SSD1306OledDebug::SSD1306OledDebug(int rPin)
{
	init(rPin, 0);
}

void SSD1306OledDebug::init(int rPin, int hOffset)
{
	resetPin = rPin;
	horizontalOffset = hOffset;

  u8g = new U8GLIB_SSD1306_128X64(U8G_I2C_OPT_NONE);

  pinMode(resetPin, OUTPUT);
  digitalWrite(resetPin, LOW);
  delay(50);
  digitalWrite(resetPin, HIGH);

  u8g->setFont(u8g_font_fixed_v0);

	clearLines();
}


void SSD1306OledDebug::clearLines()
{
	for(int i = 0; i<constants::NUMBER_OF_LINES; i++)
  {
		lines[i][0] = '\0';
	}

  u8g->firstPage();
  do {
    draw();
  } while( u8g->nextPage() );

}

void SSD1306OledDebug::appendLine(char * line)
{
	//Serial.print("Entering appendLine: ");
	//Serial.println(line);
	for(int i=constants::NUMBER_OF_LINES - 1; i>0; i--)
	{
		strcpy(lines[i], lines[i-1]);
	}

	strcpy(lines[0], line);

  u8g->firstPage();
  do {
    draw();
  } while( u8g->nextPage() );

}


void SSD1306OledDebug::draw(void)
{
  for(int i=0; i<constants::NUMBER_OF_LINES; i++)
  {
		u8g->drawStr(horizontalOffset, (constants::DISPLAY_HEIGHT-(constants::LINE_HEIGHT * i)), lines[i]);
	}	
}

