/**
 * Fill an EEPROM chip with values
 */

#include "Wire.h"

#define EEPROM_I2C_ADDRESS 0x50
#define MAX_BYTE_VAL 255
#define KB_VAL 1024

const unsigned long MEM_SIZE = 65536L;

void setup() 
{
  Wire.begin();
  Serial.begin(9600);

  writeToEeprom();
  readFromEeprom();
  
  Serial.println("Done");
}

void loop() 
{

}

/**
 * Beginning at address 0, write consecutive
 * byte values from 0-255 and then repeat until
 * EEPROM memory is full
 */
void writeToEeprom()
{
  byte value = 0;
  
  // Write a value to each of the 65,536 addresses
  for(unsigned long i=0; i<MEM_SIZE; i++)
  {
    writeAddress(i, value);
    value++;
    if(value > MAX_BYTE_VAL)
    {
      value = 0;
    }
    
    if((i % KB_VAL) == 0)
    {
      Serial.print("Wrote ");
      Serial.print((unsigned long) (i / KB_VAL));
      Serial.println(" KB");
    }
  }
}

void readFromEeprom()
{
  byte readVal = 0;
  
  for(unsigned long i=0; i<MEM_SIZE; i++)
  {
    readVal = readAddress(i);
    Serial.print("Address ");
    Serial.print(i);
    Serial.print(".      ");
    Serial.println(readVal);      
  }
}

void writeAddress(unsigned int address, byte val)
{
  Wire.beginTransmission(EEPROM_I2C_ADDRESS);

  // Shift right 8 bits to get the most significant byte
  Wire.write((unsigned int)(address >> 8));   // MSB
  
  // Mask with 0000000011111111 to get the least significant byte
  Wire.write((unsigned int)(address & 0xFF)); // LSB
  
  Wire.write(val);
  Wire.endTransmission();

  delay(5);
}

byte readAddress(unsigned int address)
{
  byte rData = 0xFF;
  
  Wire.beginTransmission(EEPROM_I2C_ADDRESS);

  // Shift right 8 bits to get the most significant byte
  Wire.write((unsigned int)(address >> 8));   // MSB

  // Mask with 0000000011111111 to get the least significant byte
  Wire.write((unsigned int)(address & 0xFF)); // LSB
  Wire.endTransmission();  

  Wire.requestFrom(EEPROM_I2C_ADDRESS, 1);  

  rData =  Wire.read();

  return rData;
}

