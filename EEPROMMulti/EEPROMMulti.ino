#include "Wire.h"

#define EEPROM_I2C_ADDRESS0 0x50
#define EEPROM_I2C_ADDRESS1 0x51
#define EEPROM_I2C_ADDRESS2 0x52
#define EEPROM_I2C_ADDRESS3 0x53

void setup() 
{
  Wire.begin();
  Serial.begin(9600);

  int address = 0;
  byte val0 = 110;
  byte val1 = 111;
  byte val2 = 112;
  byte val3 = 113;

  // Write and read from chip 0
  writeAddress(EEPROM_I2C_ADDRESS0, address, val0); 
  byte readVal = readAddress(EEPROM_I2C_ADDRESS0, address);
  
  Serial.print("The returned value is ");
  Serial.println(readVal);

  // Write and read from chip 1
  writeAddress(EEPROM_I2C_ADDRESS1, address, val1); 
  readVal = readAddress(EEPROM_I2C_ADDRESS1, address);  

  Serial.print("The returned value is ");
  Serial.println(readVal);  

  // Write and read from chip 2
  writeAddress(EEPROM_I2C_ADDRESS2, address, val2); 
  readVal = readAddress(EEPROM_I2C_ADDRESS2, address);  

  Serial.print("The returned value is ");
  Serial.println(readVal);

  // Write and read from chip 3
  writeAddress(EEPROM_I2C_ADDRESS3, address, val3); 
  readVal = readAddress(EEPROM_I2C_ADDRESS3, address);  

  Serial.print("The returned value is ");
  Serial.println(readVal);    

}

void loop() 
{

}

void writeAddress(byte i2cAddress, int address, byte val)
{
  Wire.beginTransmission(i2cAddress);
  Wire.write((int)(address >> 8));   // MSB
  Wire.write((int)(address & 0xFF)); // LSB
  
   
  Wire.write(val);
  Wire.endTransmission();

  delay(5);
}

byte readAddress(byte i2cAddress, int address)
{
  byte rData = 0xFF;
  
  Wire.beginTransmission(i2cAddress);
  
  Wire.write((int)(address >> 8));   // MSB
  Wire.write((int)(address & 0xFF)); // LSB
  Wire.endTransmission();  


  Wire.requestFrom(i2cAddress, 1);  

  rData =  Wire.read();

  return rData;
}

