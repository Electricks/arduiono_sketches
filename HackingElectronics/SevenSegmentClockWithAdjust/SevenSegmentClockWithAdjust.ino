#include <Time.h>
#include "Wire.h"
#include "Adafruit_LEDBackpack.h"
#include "Adafruit_GFX.h"

#define DS1307_I2C_ADDRESS 0x68
#define SEVENSEG_I2C_ADDRESS 0x70

#define TIME_MSG_LEN 11
#define TIME_HEADER 'T'

Adafruit_7segment matrix = Adafruit_7segment();

const int inputPin = 9;        // the number of the input pin
const int debounceDelay = 10;  // milliseconds to wait until stable

// Convert normal decimal numbers to binary coded decimal
byte decToBcd(byte val)
{
  return ( (val/10*16) + (val%10) );
}

// Convert binary coded decimal to normal decimal numbers
byte bcdToDec(byte val)
{
  return ( (val/16*10) + (val%16) );
}


// 1) Sets the date and time on the ds1307
// 2) Starts the clock
// 3) Sets hour mode to 24 hour clockrequestFrom
// Assumes you're passing in valid numbers
void setDateDs1307(byte mysecond,        // 0-59
                   byte myminute,        // 0-59
                   byte myhour,          // 1-23
                   byte dayOfWeek,     // 1-7
                   byte dayOfMonth,    // 1-28/29/30/31
                   byte mymonth,         // 1-12
                   byte myyear)          // 0-99
{
   Wire.beginTransmission(DS1307_I2C_ADDRESS);
   Wire.write(0);
   Wire.write(decToBcd(mysecond));    // 0 to bit 7 starts the clock
   Wire.write(decToBcd(myminute));
   Wire.write(decToBcd(myhour));      // If you want 12 hour am/pm you need to set
                                    // bit 6 (also need to change readDateDs1307)
   Wire.write(decToBcd(dayOfWeek));
   Wire.write(decToBcd(dayOfMonth));
   Wire.write(decToBcd(mymonth));
   Wire.write(decToBcd(myyear));
   
   byte ctrlRegByte = 0x13; // 00010000 to produce a 1 Hz square wave 
                            // from the SQW pin.  00010011 for 32.768 kHz.
                            // 00010001 for 4.096 kHz.  The square wave output
                            // can be viewed on an oscilloscope.
   Wire.write(ctrlRegByte);
   Wire.endTransmission();
}

// Gets the date and time from the ds1307
void getDateDs1307(byte *mysecond,
          byte *myminute,
          byte *myhour,
          byte *dayOfWeek,
          byte *dayOfMonth,
          byte *mymonth,
          byte *myyear)
{
  // Reset the register pointer
  Wire.beginTransmission(DS1307_I2C_ADDRESS);
  Wire.write(0);
  Wire.endTransmission();
  
  Wire.requestFrom(DS1307_I2C_ADDRESS, 7);

  // A few of these need masks because certain bits are control bits
  *mysecond     = bcdToDec(Wire.read() & 0x7f);
  *myminute     = bcdToDec(Wire.read());
  *myhour       = bcdToDec(Wire.read() & 0x3f);  // Need to change this if 12 hour am/pm
  *dayOfWeek  = bcdToDec(Wire.read());
  *dayOfMonth = bcdToDec(Wire.read());
  *mymonth      = bcdToDec(Wire.read());
  *myyear       = bcdToDec(Wire.read());
}


time_t processSyncMessage()
{
  // return the time if a valid sync message is received on the serial port.
  // time message consists of a header and ten ascii digits
  while(Serial.available() >= TIME_MSG_LEN)
  {
    char c = Serial.read();
    Serial.print(c);
    if(c == TIME_HEADER)
    {
      time_t pctime = 0;
      for(int i=0; i<TIME_MSG_LEN -1; i++)
      {
        c = Serial.read();
        if(c>='0' && c <='9')
        {
          pctime = (10*pctime) + (c-'0'); // convert digits to a number
        }
      }
      return pctime;
    }    
  }
  return 0;  
}

// debounce returns the state when the switch is stable
boolean debounce(int pin)
{
  boolean state;
  boolean previousState;

  previousState = digitalRead(pin);          // store switch state
  for(int counter=0; counter < debounceDelay; counter++)
  {
      delay(1);                  // wait for 1 millisecond
      state = digitalRead(pin);  // read the pin
      if( state != previousState)
      {   
         counter = 0; // reset the counter if the state changes      
         previousState = state;  // and save the current state
      }
  }
  // here when the switch state has been stable longer than the debounce period
  Serial.print("State is ");
  Serial.println(state);
  return state;
}

void setup()
{
  Wire.begin();
  Serial.begin(9600);
 
  matrix.begin(SEVENSEG_I2C_ADDRESS);
  
  pinMode(inputPin, INPUT);  
}

void loop()
{
  byte mysecond, myminute, myhour, dayOfWeek, dayOfMonth, mymonth, myyear, newminute;
  
 
  if(Serial.available())
  {
    time_t t = processSyncMessage();
    if(t > 0)
    {
      setTime(t);
      //adjustTime(-21600);  // Subtract 6 hours to go from GMT to US Central
      adjustTime(-18000);  // Subtract 5 hours to go from GMT to US Central DST
      
      mysecond = (byte) second();
      myminute = (byte) minute();
      myhour = (byte) hour();
      dayOfWeek = (byte) weekday();
      dayOfMonth = (byte) day();
      mymonth = (byte) month();
      myyear = (byte) year();
      //Serial.print("The year is ");
      //Serial.println(myyear);
      
      myyear -= 2000;
      
      setDateDs1307(mysecond, myminute, myhour, dayOfWeek, dayOfMonth, mymonth, myyear);      
    }
  }
  
  

  getDateDs1307(&mysecond, &myminute, &myhour, &dayOfWeek, &dayOfMonth, &mymonth, &myyear);
  
  /*
  Serial.print(myhour, DEC);
  Serial.print(":");
  Serial.print(myminute, DEC);
  Serial.print(":");
  Serial.print(mysecond, DEC);
  Serial.print("  ");
  Serial.print(mymonth, DEC);
  Serial.print("/");
  Serial.print(dayOfMonth, DEC);
  Serial.print("/");
  Serial.print(myyear, DEC);
  Serial.print("  Day_of_week:");
  Serial.println(dayOfWeek, DEC);
  */
  
  int decimalTime = (myhour * 100) + myminute;
  
  matrix.print(decimalTime);
  matrix.drawColon(true);
  matrix.writeDisplay();
  
  int state = debounce(inputPin);
  if(state == LOW)
  {
    newminute = myminute + 1;

    if(newminute >= 60)
    {
      newminute = 0;
    }
    
    myyear -= 2000;
    
    setDateDs1307(mysecond, newminute, myhour, dayOfWeek, dayOfMonth, mymonth, myyear);    
    
  }

  delay(200);
}




// Stops the DS1307, but it has the side effect of setting seconds to 0
// Probably only want to use this for testing
/*void stopDs1307()
{
  Wire.beginTransmission(DS1307_I2C_ADDRESS);
  Wire.write(0);
  Wire.write(0x80);
  Wire.endTransmission();
}*/
