#include <SPI.h>
#include <Ethernet.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS 3
#define TEMPERATURE_PRECISION 12

float tempC, tempF;
float sumTempC, sumTempF;
float avgTempC, avgTempF;
float minTempF, maxTempF;

OneWire oneWire(ONE_WIRE_BUS);

DallasTemperature sensors(&oneWire);

//Permanent Board
DeviceAddress thermoOne = {
  0x28, 0x20, 0x05, 0xB4, 0x04, 0x00, 0x00, 0x7B};
DeviceAddress thermoTwo = {
  0x28, 0xBC, 0xBA, 0xB4, 0x04, 0x00, 0x00, 0x97};

byte mac[] = {
  0x48, 0xC2, 0xA1, 0xF3, 0x8D, 0xB7};
byte ip[] = {
  192,168,1,147};

EthernetServer server = EthernetServer(80);

void setup()
{
  Serial.begin(9600);
  
  minTempF = -1.0;
  maxTempF = 500.0;

  Ethernet.begin(mac, ip);
  server.begin();
  sensors.begin();

  sensors.setResolution(thermoOne, TEMPERATURE_PRECISION);
  sensors.setResolution(thermoTwo, TEMPERATURE_PRECISION);
}

void getTemperature(DeviceAddress deviceAddress)
{
  tempC = sensors.getTempC(deviceAddress);
  tempF = DallasTemperature::toFahrenheit(tempC);
}

void loop() 
{
  sensors.requestTemperatures();

  sumTempC = sumTempF = 0.0;
  
  getTemperature(thermoOne);
  sumTempC += tempC;
  sumTempF += tempF;
  getTemperature(thermoTwo);
  sumTempC += sumTempC;
  sumTempF += sumTempF;          
  avgTempC = sumTempC / 2.0;
  avgTempF = sumTempF / 2.0;

  if(minTempF < 0.0)
    minTempF = avgTempF;
  else if(minTempF > avgTempF)
    minTempF = avgTempF;

  if(maxTempF > 499.0)
    maxTempF = avgTempF;
  else if(maxTempF < avgTempF)
    maxTempF = avgTempF;

  EthernetClient client = server.available();
  if(client)
  {
    boolean BlankLine = true;
    while(client.connected())
    {
      if(client.available())
      {
        char c = client.read();

        if(c == '\n' && BlankLine)
        {
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html\n");
          client.println("<html><head><META HTTP-EQUIV=""refresh"" CONTENT=""5"">\n");
          client.println("<title>Arduino Web Server</title></head>");
          client.println("<body style=\"background-color:#5EA084\">\n");
          client.println("<h1>Arduino Web Server</h1>");
          client.println("<h3>Current Temperature</h3>");

          client.println(avgTempF);
          client.println("&deg;F");                              
          client.println("<br><br><br>");
          client.println("<h4>Minimum Temperature</h4>");
          client.println(minTempF);          
          client.println("&deg;F");                    
          client.println("<br>");          
          client.println("<h4>Maximum Temperature</h4>");
          client.println(maxTempF); 
          client.println("&deg;F");          
          client.println("</body>\n</html>");

          break;
        }
        if(c == '\n')
        {
          BlankLine = true;
        }
        else if(c != '\r')
        {
          BlankLine = false;
        }
      }
    }
    delay(10);
    client.stop();
  }
}



